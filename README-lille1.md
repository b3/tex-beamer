# Modèles de présentations pour Lille 1

**Attention l'Université de Lille 1 n'existe plus (fusionnée dans l'Université de Lille). Le thème et les modèles n'évoluent donc plus.**

Des fichiers présents ici s'adressent aux personnes qui veulent des modèles de
présentation *propres* (c'est-à-dire avec exigence sur la qualité du résultat)
utilisant l'identité de l'[Université de Lille 1](http://www.univ-lille1.fr).

Ça s'adresse aussi aux personnes qui ne connaissent pas très bien LaTeX et qui
débutent dans son utilisation ou dans l'utilisation de Beamer. L'objet est de
faciliter la production de support de présentation (*Powerpoint* en novlangue)
avec un rendu simple mais spécifique.

Pour ceux qui connaissent [LaTeX](http://www.latex-project.org) et sa classe
[Beamer](https://github.com/josephwright/beamer), les fichiers importants sont
le thème Beamer [`beamerthemelille1.sty`](etc/beamerthemelille1.sty)
(`lille1`) et le logo vectoriel de l'Université de Lille 1
[`logo-univ-lille1.pdf`](img/logo-univ-lille1.pdf).

Pour les *débutants* pour bien utiliser ces modèles il est recommandé de lire
d'abord [modele-cours-lille1.pdf](modele-cours-lille1.pdf) puis de le relire
une seconde fois en comparant avec le contenu du fichier source
[modele-cours-lille1.tex](modele-cours-lille1.tex).

La même chose peut-être faite avec
[modele-presentation-lille1.pdf](modele-presentation-lille1.pdf) et
[modele-presentation-lille1.tex](modele-presentation-lille1.tex).

Pour ceux qui ont des besoins encore plus rapide et léger (présentations très
simples), le script [md2beamer](bin/md2beamer) permet de transformer un
fichier Markdown en diaporama PDF grâce à beamer. Un exemple *explicatif* est
disponible dans [modele-diaporama.md](modele-diaporama.md).
